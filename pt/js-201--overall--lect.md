# More

array functions
prototype (Array.prototype.recude.call)
use strict

---

# ES6

ES 6 = ECMA 2015

First major update since ES5 (2009)

---

# Goals

- Fix issues from ES5
- Backward compatibility
- Modern Syntax
- Better Scaling
- Updated Standard Library

---

# Transpiler

Compiles ES6 to ES5.

- Babel
- Traceur
- Closure

---

# What's New?

- `let` and `const`
- Destructuring assignment
- Classes and Inheritance (vs prototype-based)
- Template Strings
- String Features
- Math & Number Features
- New Data Structures
- Iterators
- Generators
- Promises & Asynchronous Data
- Arrow Functions (+lexical `this`)

---

## let

A block-scope variable

```js
if(true)
{
  var a = 'mike'
  let b = 'jane'
}

console.log(a, b);
```

---

## Did you know?

```js
for(var i=0; i<10; i++) {
  console.log(i);
}

console.log(i);
```

---

## Examples

```js
{
  let name = 'daniel'
}

console.log(name)
```

```js
for(let i=0; i<10; i++) {
  console.log(i);
}

console.log(i);
```

---

# Const

```js
const KEY = 10
console.log(KEY++)
```

---

# Template Literals

Back-tick String

**Multi-Line**
```js
let name = `batman
and robin`;
console.log(name);
```

**Easy Concatenation**
```js
let first = "Steve";
let last = "Jobs";

console.log(`My name is ${first} ${last}`)
```

--- 

## Another Example

```js
let person = {
  first: 'john',
  last: 'johnson',
  getName() {
    return `${this.first} ${this.last}`;
  }
}

let name = person.getName()
console.log(name)
```

---

# Arrow Function

Inspired by `coffeescript`

```js
var add = (a,b) => a + b

console.log( add(1,2) )
```

---

## Function Declaration

```js
function add(a, b) {
    return a + b;
}

let add = function(a, b) {
    return a + b;
}

let add = (a, b) => {
    return a + b;
}
```

---

## Shortened forms

```js
let double = (a) => {
    return a * 2;
}

let double = a => a * 2
```

- If there is only 1 parameter, the parenthesis can be skipped
- If there is only 1 (return) statement, the curly brackets can be skipped

---

## Example

```js
let numbers = [2, 3, 4, 5, 6, 7]

let doubled = numbers.map(function(n) {
  return n * 2
})

console.log(doubled)
```

Converted to an arrow function.

---

## Using an arrow function

```js
let numbers = [2, 3, 4, 5, 6, 7]

let doubled = numbers.map(n => n * 2)

console.log(doubled)
```

---

## With objects

```js
let person = {
  name: 'Daniel',
  greet: function() {
    console.log(`Hi my name is ${this.name}`)
  },
  greet2() {
    console.log(`Hi my name is ${this.name}`)
  },
  greet3: () => console.log(`Hi my name is ${this.name}`)
}

person.greet()
person.greet2()
person.greet3()
```

---

# lexical `this`

```js
let person = {
  name: 'Daniel',
  greet() {
    setTimeout(function() {
      console.log(`hi my name is ${this.name}`)
    }, 1000);
  }
}

person.greet()
```

---

## Solutions

```js
greet() {
    setTimeout(function() {
      console.log(`hi my name is ${this.name}`)
    }.bind(this), 100);
}
```

```js
greet() {
    let that = this
    setTimeout(function() {
      console.log(`hi my name is ${that.name}`)
    }, 100);
}
```

```js
  greet() {
    setTimeout(() => console.log(`hi my name is ${this.name}`), 1000);
}
```

---

# Rest Parameters

```js
let sum = function() {
  console.log(arguments);   // argument is a array-like object
  return Array.prototype.reduce.call(arguments, (prev, curr) => prev + curr)
};

console.log( sum(2, 3, 4, 5, 6));
```

```js
let sum = function(...args) {
  console.log(args);
  return args.reduce((prev, curr) => prev + curr)
};

console.log( sum(2, 3, 4, 5, 6));
```

---

## Multiply Example

```js
console.clear()

let multiply = (mul, ...numbers) => {
  console.log(mul);
  console.log(numbers);
  return numbers.map(n => mul * n)
}

var result = multiply(2, 7, 4, 5);
console.log("result", result);
```

---

# Spead Operator

Let's try

```js
let numbers = [4, 6, 3, 8]

let max = Math.max(numbers);

console.log(max)
```

What we need is `Math.max(4, 6, 3, 8)`

---

## Solutions

**1. apply**

```js
let numbers = [4, 6, 3, 8]

let max = Math.max.apply(null, numbers);

console.log(max)
```

**2. Spread**

```js
let numbers = [1, 4, 3, 5]

let max = Math.max(...numbers);

console.log(max);
```

---

## Array Concat

```js
let ar1 = [1, 4, 3, 5]
let ar2 = [10,15,20,22]
let concat = ar2.concat(ar1)
console.log(concat)

let ar3 = [10, 15, 20, 22, ...ar1]
console.log(ar3)
```

---

# Destructuring

```js
let person = {
  age: 30
}

let a = person.age
let b = person["age"]
let { age: c } = person

console.log(a, b, c)
```

Multiple keys

```js
let person = {
  age: 30,
  name: 'john',
  location: 'washington'
}

let { age, location: curLocation } = person

console.log(age, curLocation)
```

And similar to `obj['key']`

```js
let person = {
  age: 30,
  name: 'john',
  location: 'washington'
}

let key = "age";
let { [key]: myAge } = person

console.log(myAge)
```

Desctructuring an array

```js
let numbers = [1,2,3,4]
let [first, second] = numbers
let [first, second, , fourth] = numbers
```

import

```js
var fn1 = require('..').fn1;
var fn2 = require('..').fn2;
var fn3 = require('..').fn3;
```

vs

```js
var {fn1, fn2, fn3} from '...'
```

---

# Promises

States

- Pending
- Resolved
- Rejected

```js
let p = new Promise((resolve, reject) => {
    resolve('good to go!');
});

p.then(ret => console.log("received", ret));
```

```js
let p = new Promise((resolve, reject) => {
    setTimeout(()=>resolve('Good To Go!'), 1000)
});

p.then(ret => console.log("received", ret));
```

Cannot do

```js
resolve('1');
reject('1');
```

Comprehensive Example

```js
let p = new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Math.random() < 0.5) {
        resolve("Good To Go!")
      } else {
        reject("Uh Oh")
      }
    }, 100)
});

p.then(
    res => console.log("received", res),
    err => console.error("error received", err)
  )

p
  .then(
    res => console.log("received", res)
  )
  .catch(
    err => console.error("error received", err)
  )
```

---

## Multiple Promises

```js
let p = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Good To Go!"), 100)
});

let q = new Promise((resolve, reject) => resolve("Also good to go!"))

Promise.all([p, q]).then((data) => console.log(data))
```

```js
let p = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Good To Go!"), 100)
});

let q = new Promise((resolve, reject) => reject("Something went wrong"))

Promise.all([p, q])
  .then(data => console.log(data))
  .catch(err => console.error(err))
```

---

# New `fetch` API

```js
fetch('http://api.icndb.com/jokes/random/10')
  .then(res => {
    // console.log(res)
    res.json().then(data => {
      console.log(data)
    })
  })
  .catch(err => {
    console.error(err.message)
  })
```