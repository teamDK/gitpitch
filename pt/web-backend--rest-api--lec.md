# Rest API

---
# SOAP vs RESTful

- SOAP
	- protocol
- RESTful
	- architectural style
	- Can use any protocol (HTTP, SOAP)

---
# SOAP vs RESTful

| Operation | SOAP approach | RESTful approach |
|---|--|--|
| Create item | CreateItem(string id, string title) | **PUT** /item.svc/{id} |
| Update item | UpdateItem(string id) | **PUT** /item.scv/{id} |
| Get item | GetItem(string id) | **GET** /news.scv/{id} |
| Get items | GetItems() | **GET** /news.scv/ |
| Delete | DeleteNewsItem(string id) | **DELETE** /news.scv/{id} |

---
# What is REST?

> **RE**presentational **S**tate **T**ransfer

What defines the basis of RESTful-style?  
The REST architectural style describes six constraints

1. Uniform Interface
2. Stateless
3. Cacheable
4. Client-Server
5. Layered System
6. Code on Demand

--- 
# Resource Based

- **Things** vs actions
	- **Noun** vs Verbs
- Identified by URIs
	- Multiple URIs may refer to same resource
- Separate from their representations.

---
# Representations

- Part of the resource state
	- Transfered between client and server
- Typically JSON or XML
- Example
	- Resource: person (Todd)
	- Service: contact information (GET)
	- Representation
	- name, address, phone number
	- JSON or XML format

---
# Constraint 1 : Uniform Interface

- Defines the interace between client
- Simplified and decouples the architecture
- Fundamental to RESTful design
- e.g.:
	- HTTP verbs (GET, PUT, POST, DELETE)
	- URIs (resource name)
	- HTTP response (status, body)

---
# Constraint 2 : Stateless

- Server contains no client state
- Each request contains enough context to process the message
	- Self-descriptive messages
- Any session state is held on the client

---
# Constraint 3 : Client-Server Architecture

- Assume a disconnected system
- Separation of concerns
- Uniform interface is the link between the two

---
# Constraint 4 : Cacheable

- Serve responses (representations) are cacheable
	- Implicitly
	- Explicitly (max age, duration)
	- Negotiated

---
# Constraint 5 : Layered System

- Client cannot assume direct connection to server
- S/W or H/W intermediaries between client and server
- Improves scalability

---
# Constraint 6 : Code on Demand

- Server can temporarily extend client
- Transfer logic to client
- Client executes logic
- e.g.:
	- JavaScript
- The only optional constraint

---
# Summary

- Violating any constraint means service is not strictly RESTful
	- REST-like
- Compliance with REST constraints allows
	- Scalability
	- Simplicity
	- Modifiability
	- Visibility
	- Portability
	- Reliability

---



