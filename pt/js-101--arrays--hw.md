# Arrays Homework

Given the following array, 

```js
var ar = [ 1, 3, 11, 4, 7, 8, 10, 20, 14];
```

Write the following functions that:

Q1. Finds a number(s) that is bigger than its next number.

```js
biggerThanNext(ar);     // output: 11, 20
```

Q2. Finds the maximum

```js
findMax(ar);        // output: 20
```

Q3. Finds the minimum

```js
findMin(ar);        // output: 1
```

Q4. Swaps the max with the min

```js
swapMinMax(ar);     // output: [ 20, 3, 11, 4, 7, 8, 10, 1, 14]
```

Q5. Finds any adjacent pair(s) whose sum is 15

```js
pair15(ar);     

// output: 
// 11, 4
// 7, 8
```

Q6. Creates another array consisting of even numbers from `ar`

```js
var evenAr = clonEven(ar);
console.log(evenAr);    // output: [ 4, 8, 10, 20, 14]
```