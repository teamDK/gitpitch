# Advanced Homework Quizzes

Q1. Complete the function `fn()`

```js
fn()()      // prints "hello"
```

Q2. Given an array of `student` objects, print the names of students who scores 90 or better

```js
var students = [
    {name: "Julie", score: 23},
    {name: "John", score: 93},
    {name: "Jane", score: 13},
    {name: "Natalie", score: 99},
    {name: "Mike", score: 50},
    {name: "Danielle", score: 73}
]
```

Q3. Write a function that clones an array (use `map()` if you learnt it)

```js
var ar = [1,3,5]
var ar2 = clone(ar)
```

Q4. Create a `Person` class

```js
var person = new Person("john", 10);
person.greet(); // prints "Hi my name is John and I am 10 years old"
```

Q5. Write a function `findMax(ar, n)` which finds the `n`th largest number from `ar`

```js
findMax([1, 3, 5, 7, 9], 3);        // returns 5
```