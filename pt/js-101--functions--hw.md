# JS-101 Functions Quiz

Implement the following 4 functions in a file `functions.js` then submit the file.

## Q1. pyramid

**input**
```js
pyramid(3);

pyramid(5);
```

**output**
```language-none
1
22
333

1
22
333
4444
55555
```

## Q2. series

**input**
```js
series(10, 15);

series(100, 102);
```

**output**
```language-none
10
11
12
13
14
15

100
101
102
```

## Q3. max

**input**
```js
var x = max(5, 3);
var y = max(1, 7);
console.log(x, y);
```

**output**
```language-none
5 7
```

## Q4. even

**input**
```js
even(4);
even(10);
even(3);
```

**output**
```language-none
4 is an even number
10 is an even number
3 is not an even number
```