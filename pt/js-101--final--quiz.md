# Q1. `Car` Object

- The `car` object has a default `speed` property of value 1
- The `car` object is initially at x = 0
-  `move()` function moves the car by its speed +/- 3

```js
var car = {
    // fill in
}

car.speed = 10;

for (var i = 0; i < 5; i++) {
    car.move();
}
```

## Expected output

```language-none
car is at x = 10
car is at x = 19
car is at x = 29
car is at x = 30
car is at x = 41
```

# Q2. Array Generator

`ag` is an array generator object.

`ag.generate()` takes 3 parameters a, b, c and returns an array such that

- a - size of the returned array  
- b - minimum number generated
- c - maximum number generated

```js
var ag = {
    // fill in
}

var ar = ag.generate(3, 2, 5);
console.log(ar);
```

## output

```language-none
[ 2, 5, 3]
```

# Q3. Reverse

Generate a reversed array using only `push()` and `pop()`.
No square bracket is allowed

```js
function reverse(ar) {
    // fill in
}

var ar = reverse( [1, 3, 2, 6]);
console.log(ar);
```

## expected output

```language-none
[6, 2, 3, 1]
```