# Javascript 101 - Conditionals

`IF` and `Switch`

---

# IF

```js
var score = 80;

if (score >= 50) 
{
    console.log("pass");
}
```

Quiz: Print `fail` if score < 50

---

# ELSE

```js
var score = 80;

if (score > 50)
{
    console.log("pass");
}
else 
{
    console.log("fail");
}
```

---

# ELSE-IF

```js
var score = 80;

if (score > 90) 
{
    console.log("Extra Credit");
}
else if (score > 50)
{
    console.log("Pass");
}
else
{
    console.log("Fail");
}
```

Quiz: Rewrite the above code snippet using only the `if` keyword

---

## Quiz

Given a `score` variable, print the letter grade based on the following table

| score | grade |
|---|---|
| 90 ~ 100 | A |
| 80 ~ 89 | B |
| 50 ~ 79 | C |
| 0 ~ 49 | D |

---

## Quiz

1. Given an `age` variable, print `teen` only if the age is between 10 and 19 (inclusive).

2. Given an `age` variable, print `not teen` only if the age is not in the range 10~19 (inclusive).

3. Merge the answers from 1 and 2 into one `if` statement.

---

## Discuss

```language-none
if (1 < x < 10) {
    ....
}

if (x > 10 || < 20) {
    ....
}

if (x == 20 || 30) {
    ....
}

if ( x = 20 ) {
    ....
}

if (x < 10 && x > 20) {
    ....
}

if (x > 10 || x < 20) {
    ....
}
```

---

## Discuss

```js
if (age >= 10 && age < 20)
{
    console.log("teen");
}
else
{
    console.log("adult");
}
```

---

# Switch

```js
switch(rank) 
{
  case 1:
    console.log("Gold");
    break;
  case 2:
    console.log("Silver");
    break;
  case 3:
    console.log("Bronze");
    break;
  default:
    console.log("Thank you for participating")
}
```

---

## Quiz

```js
switch(number) 
{
    case 1: 
    case 2: 
        console.log(2);
    case 3: 
        console.log(3);
        break;
    default: 
        console.log(4);
}
```

What is the output when the value of `number` is 1, 2, 3 and 4?

---

# Comments

```js
// one liner

/*
multi liner
*/
```