# CSS

Cascading Style Sheet

---

# Syntax

```css
div {
    color: red;
    background-color: blue;
}
```

- selector
    - property: value pair
    - semi-colon

---

# How to apply

## inline

```html
<div style="color: red">Hello World</div>
```

## internal style sheet

```html
<style>
    div {
        color: red;
    }
</style>
```

## external style sheet

```css
/* style.css */
div {
    color: red;
}
```

```html
<!-- index.html -->
<link rel="stylesheet" type="text/css" href="style.css">
```

---

# CSS Cheatsheet

[cheatsheet](https://docs.google.com/document/d/1lMu0sc7_47FQFVUQHFKDTpV6Q17WvDQKP62ZN4NQ5ps/edit?usp=sharing)

---

# CSS Selectors

[cheatsheet](https://docs.google.com/document/d/18MNQ9Ss-VlcQXqlf9F-LhFZc1dlqhi2C38hw2T4XC5k/edit?usp=sharing)

---

## Quiz

How many elements are affected by this css selector

```html
<h1>
    <div class="menu1">1</div>
    <div class="menu2">1</div>
</h1>

<h2>
    <div class="menu1">1</div>
    <div class="menu2">1</div>
</h2>
```

```css
h1.menu1, .menu2 { background-color: pink; }
```

---

# Selectors

even / odd
```css
tr:nth-child(even) {}
tr:nth-child(2n) {}

tr:nth-child(odd) {}
tr:nth-child(2n+1) {}
```

---

## QUIZ

[http://jsbin.com/gohika](http://jsbin.com/gohika/)

Change the CSS tab only:

1. Change all `<li>` to have **bold** text
1. Change font colors in the second list to orange
1. Change font sizes of all `.us` cars to be 1.5 em
1. Remove all bullets for `.us` cars
1. Change colors of all `kia` cars to green. (Use their positions relative to `.us` cars)
1. Change all cars below `.us` cars to italic.

---

## QUIZ

[http://jsbin.com/zubomox](http://jsbin.com/zubomox/)

Change the CSS tab only:

- Change colors of all header text to orange and Arial font.
- Change background colors of all first children (Quackmore, Donald, and Huey) to skyblue
- Change background colors of all non-first children to beige
- Change background colors of all `<div>` tags that doesn't have the `.duck` class to #eee
- How would you change the code the get the outer most border back?

```css
div:first-of-type {
    border: none
}
```

- Predict the result of the following codes