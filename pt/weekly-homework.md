## Weekly Homework

Last updated - 2017 / 7 / 6

# Beginners

Q1. Write a loop that prints the following

```language-none
1
3
5
7
9
```

Q2. Print all 2-digit numbers that are divisible by either 4 or 9. (For example, 12, 16, 18, 20....)

Q3. Calculate the sum of all numbers from 10 to 20.

Q4. Print the following

```language-none
1 10
2 9
3 8
4 7
5 6
6 5
7 4
8 3
9 2
10 1
```

Q5. Print all integers from 1 to 100.  
Replace any number divisible by three with the word "Fizz", and any number divisible by five with the word "Buzz". (15 will be `Fizz Buzz`)

```language-none
1
2
Fizz
4
Buzz
Fizz
..
```

# Arrays

Q6. Find the second largest number from an array

```js
findMax2([1, 5, 2, 7, 3, 4]);       // answer: 5
```

Q7. Find the third largest number from an array

```js
findMax3([1, 8, 4, 9, 3, 2, 1]);    // answer: 4
```

Q8. Find the number that is out-of-order in an otherwise sorted array

```js
findUnsorted([1, 2, 4, 8, 10, 9, 12]);  // answer: 9
```

# Answers

Q1. 

```js
// Q1
var i = 1;

while(i <= 9) {
  console.log(i);
  i=i+2;
}


// Q1 - for loop version
for(var i=1; i<=9; i=i+2) {
  console.log(i);
}
```

Q2.

```js
for (var i = 10; i < 100; i++) {
  if (i % 4 == 0) {
    console.log(i);
  } else if (i % 9 == 0) {
    console.log(i);
  }
}
```

Q3.

```js
var sum = 0
var num = 10

while (num <= 20) {
    sum = sum + num
    num++
}

console.log(sum)
```

Q4.

```js
var a = 1
var b = 10

while(a <= 10)
{
    console.log(a, b)
    a++
    b--
}
```

Q5. 

```js
for (var i = 0; i <= 100; i++) {
    if (i % 15 == 0) {
        console.log("Fizz Buzz")
    }
    else if (i % 3 == 0) {
        console.log("Fizz")
    }
    else if (i % 5 == 0) {
        console.log("Buzz")
    }
    else {
        console.log(i)
    }
}
```