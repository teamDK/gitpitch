# Express.js

## Prerequisites
- node.js
- npm


```bash
npm install -save express
npm install -g nodemon
```

---

# Hello World

```js
var express = require('express');
var app = express();

app.get('/', function(req, res){
    res.send("Hello world!");
});

app.listen(3000);
```

```bash
nodemon index.js
```

---

# Routing

## app.METHOD(path, handler)

- Method: HTTP Verbs (get, post, put, delete)

```js
app.get('/', function(req, res) { /* ... */  } );
app.post('/', function(req, res) { /* ... */  } );
app.all('/', function(req, res) { /* ... */  } );
```

---

## Router

## index.js
```js
var express = require('express');
var app = express();

var person = require('./route_person.js'); 
app.use('/person', person); 

app.listen(3000);
```

## route_person.js
```js
var express = require('express');
var router = express.Router();

// define router here

module.exports = router;
```

## Example 
```js
var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
	res.send('url:  /person/');
});
router.get('/1', function(req, res){
	res.send('url: /person/1');
});

module.exports = router;
```

---

# URL Params

```js
app.get('/:id', function(req, res) {
    var id = req.params.id;

});
```

```js
app.get('/:id/:name', function(req, res) {
    var id = req.params.id;
    var name = req.params.name;
})
```

---

## Pattern matching

```js
// will match /12345
// will NOT match /123456
app.get('/:id([0-9]{5})', function(req, res){
    var id = req.params.id;
});

// Matches all
// There for place after all other routes
app.get('*', function(req, res){

});
```

---

# Middleware

# Middleware

# Middleware

---

# Middleware

```js
var express = require('express');
var app = express();

//Simple request time logger
app.use(function(req, res, next){
	console.log("Req received at " + Date.now());
	next();
});

app.listen(3000);
```

