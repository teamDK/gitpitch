# JS 101 - Arrays

---

# Initializing an array

```js
var ar = [];

var ar = [1,2,3,4];

var ar = new Array(5);

var ar = new Array(1,2,3,4,5);  // BAD
```

```js
console.log(ar);
```

---

# Read / Write

```js
var ar = [1,3,5,7,9];

ar[0] = 10; // WRITE

console.log(ar[0]); // READ

console.log(ar);
```

---

# Index and Length

```js
var ar = [1,3,5,7,9];

// length
console.log( ar.length );

// first element
console.log( ar[0] );

// last element
console.log( ar[4] );
console.log( ar[ar.length-1] );
```

---

# Javascript Array Properties

Type agnostic

```js
var ar = [10, 'string', true];
```

Dynamic Length

```js
var ar = [];
ar[5] = 2;
```

---

# Iteration

```js
for (var i = 0; i <= ar.length - 1; i++) {
    console.log(ar[i]);
}
```

```js
for (var i = ar.length - 1; i >= 0; i--) {
    console.log(ar[i]);
}
```

---

## Example - SUM

```js
var sum = 0;
for (var i = 0; i < ar.length; i++) {
    sum = sum + ar[i];
}
console.log(sum);
```

---

## Example - MAX

```js
var max = ar[0];
for (var i = 1; i < ar.length; i++) {
    if (ar[i] > max) {
        max = ar[i];
    }
}
```

---

# QUIZ

- Print even numbers

- Generate an array of 20 random numbers (each random number is between 0 ~ 10)

- Reverse the array

- Find Min/Max (1 loop)

- Swap Min with Max

- Print all numbers that are bigger than their previous numbers
