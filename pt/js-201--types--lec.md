# Javascript Types

---

# 7 Data Types

## 6 primitive types

- Boolean
- Number
- String
- Null
- Undefined
- Symbol

## 1 non-primitive type

- Object

---

# Null vs Undefined

- **Undefined** - A varible/object is declared but **not initialized**
- **Null** - A special value assigned to a variable as a representation of no value.

```js
var a

console.log(typeof a)   // 'undefined'
console.log(a === null) // false
console.log(a === undefined)    // true
```

```js
var a = null

console.log(typeof a)   // object
console.log(a === null) // true
console.log(a === undefined)    // false
```

## Careful..

```js
null === undefined  // false
null == undefined   // true
```

## How to check

```js
if (typeof a === 'undefined') { /*...*/ }   // undefined only
if (a === null) { /*...*/ } // null only
if (a == null) { /*...*/ }  // both null & undefined
if (!a) { /*...*/ } // null & undefined & 0 & false & ...
```

---

# Special Number Values

## Infinity

```js
var a = 1 / 0
var b = Infinity
console.log(a === b)    // true
```

## NaN

```js
var a = 1/'a'
console.log(a)  // NaN
console.log(isNaN(a))   // true
```

---

# Fundamental objects

- Object
- Function
- Boolean
- Error
- String
- Number
- **Date**
- **RegExp**
- More...

# Collections

- Array
- Map
- Set
- More...

---

# Date

```js
new Date();
new Date(value);
new Date(dateString);
new Date(year, month[, date[, hours[, minutes[, seconds[, milliseconds]]]]]);
```

## Methods

```js
Date.now()
Date.parse()
Date.UTC()
```

## Examples

```js
var today = new Date();
var birthday = new Date(1995, 11, 17, 3, 24, 0);    // December 17

today.setFullYear(2016);
```

```js
var start = Date.now();

setTimeout(function() {
    var end = Date.now();
    console.log("elapsed", end - start);    // approx 1000 milliseconds
}, 1000);
```
