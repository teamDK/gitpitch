Print the following sequence.

Q1.

```language-none
1
2
3
4
5
```

Q2.

```language-none
15
14
13
12
11
```

Q3.

```language-none
1
3
5
7
9
```

Q4.

```language-none
1
10

2
20

3
30

4
40

5
50
```

Q5.

```language-none
*****
```

Q6.

```language-none
*
**
***
****
*****
```