## Q1. 

Create a nodejs server (`index.js`) that can successfully host 3 static files. Submit the `index.js` file

- filename (content type)
- index.html (text/html)
- style.css (text/css)
- script.js (application/javascript)
