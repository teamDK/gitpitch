1
select t.name, year, win, loss 
from team_stat s, team t
where s.team_id = t.id
order by win desc

2
select t.name, year, sum(win), sum(loss) from team_stat s, team t
where s.team_id = t.id
group by t.id
order by sum(win) desc

Bulls	2000	149	91
Thunder	2000	132	108
Suns	2000	122	118
Lakers	2000	112	128

3.
select name, points/games ppg, year from player_stat s, player p
where s.player_id = p.id
order by 
ppg desc

dennis rodman	34.7143	2001
dennis rodman	34.1791	2000
michael jordan	31.8667	2000
lebron james	31.5286	2001
albert pujols	29.8209	2000
kobe bryant	26.6269	2001
albert pujols	26.4407	2002
shaquille oneal	26.1081	2000
mike trout	26.0192	2000
robert horry	25.8113	2001
scottie pippen	25.0429	2001
scottie pippen	24.9859	2002
kobe bryant	24.9589	2000
kyrie irving	24.7945	2000
robert horry	23.9091	2000
lebron james	21.7586	2000
kevin love	21.0164	2002
kevin love	19.9524	2001
kyrie irving	18.5500	2002
mark mcgwuire	18.4426	2001
mark mcgwuire	16.8548	2002
mike trout	16.6000	2001
kobe bryant	16.0923	2002
mike trout	16.0690	2002
mark mcgwuire	15.9467	2000
kevin love	15.8966	2000
scottie pippen	15.1053	2000
michael jordan	13.8158	2001
kyrie irving	13.5094	2001
lebron james	13.0746	2002
shaquille oneal	12.6071	2002
albert pujols	12.3514	2001
dennis rodman	11.9726	2002
shaquille oneal	11.1846	2001
michael jordan	10.7662	2002
robert horry	10.2179	2002

4.
select name, sum(points)/sum(games) ppg from player_stat s, player p
where s.player_id = p.id
 group by p.id
order by ppg desc

dennis rodman	26.6381
kobe bryant	22.6927
albert pujols	22.3600
lebron james	22.2821
scottie pippen	21.5438
kyrie irving	19.5645
mike trout	19.2229
kevin love	19.0165
robert horry	19.0000
michael jordan	18.7237
shaquille oneal	17.2564
mark mcgwuire	17.0000

5. career high seasons.
select p.name, max(points/games)
from
	player p,
    player_stat ps
where
	p.id = ps.player_id
group by p.id


10.

select 
	t.name,
    (
		select p.name
        from
			player p,
            player_stat ps
		where
			p.id = ps.player_id and
            p.teamid = t.id
		order by ps.points/ps.games desc
        limit 1
    )
from team t



Bulls	2002	scottie pippen
Lakers	2002	kobe bryant
Suns	2002	kevin love
Thunder	2002	albert pujols

