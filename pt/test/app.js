var http = require("http");
var fs = require("fs");

var server = http.createServer((req, res) => {

    console.log(req.url);

    if (req.url == "/") {
        serveStatic('/index.html', res);
    }
    else if (/\.(html|css|js)$/i.test(req.url)) {
        serveStatic(req.url, res);
    }
    else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end('Requested resource does not exist');
    }
}).listen(3000);

console.log("Server listening on port 3000");

function serveStatic(url, res) {

    var ext = url.split('.').pop()
    var filename = url.split('/').pop();

    var contentType = 'text/plain';

    switch(ext) {
        case 'html': contentType = 'text/html'; break;
        case 'css': contentType = 'text/css'; break;
        case 'js' : contentType = 'application/javascript'; break;
    }

    console.log(filename, contentType);

    res.writeHead(200, { 'Content-Type': contentType });
    fs.readFile(filename, function(err, data) {
        res.end(data);
    })

}