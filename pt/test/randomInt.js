module.exports = {
    getRandInt: function(max) {
        return Math.floor(Math.random() * max)
    },

    getRandRange: function(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}