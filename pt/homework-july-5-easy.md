# Easy

### Q1.

Print 

```language-none
1 2 3
2 3 4
3 4 5
4 5 6
5 6 7
6 7 8
7 8 9
8 9 10
9 10 11
10 11 12
```

### Q2.

Print

```language-none
1 2 3
4 5 6
7 8 9
10 11 12
```

### Q3.

Print

```language-none
1 11
2 22
3 33
4 44
5 55
6 66
7 77
8 88
9 99
10 110
```

### Q4.

Print

```language-none
1 1
2 4
3 7
4 10
5 13
6 16
7 19
8 22
9 25
10 28
```

### Q5.

Print

```language-none
0 1
2 0
0 3
4 0
0 5
6 0
0 7
8 0
0 9
10 0
```

# Answers

### Q1.

```js
var i = 1

while(i <= 10)
{
  console.log(i, i+1, i+2)
  
  i++
}
```

### Q2.

```js
var i = 1

while(i <= 10)
{
  console.log(i, i+1, i+2)
  
  i += 3
}
```

### Q3.

```js
var i = 1

while(i <= 10)
{
  console.log(i, i*11)
  
  i++
}
```

### Q4.

```js
var a = 1
var b = 1

while(a <= 10)
{
  console.log(a, b)
  
  a++
  b = b + 3
}
```

### Q5.

```js
var a = 1

while(a <= 10)
{
  if (a %2 == 0) 
  {
    console.log(a, 0);
  } 
  else 
  {
    console.log(0, a);
  }
  
  a++
}
```