Use this array for all questions.

```js
var ar = [1, 7, 3, 8, 1, 10, 8]
```

### Q1

find the max and the min. Use 1 loop only.

### Q2

Find the sum of all numbers

### Q3

Find the sum of all `odd` numbers

### Q4

Find the second largest number

### Q5

Find all numbers that occur more than once

### Q6

Find the average of all `even` numbers