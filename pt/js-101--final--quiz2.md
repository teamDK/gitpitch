# Q1 - Find duplicate

Find any one duplicate pair in an array.

```javascript
var ar = [1, 5, 2, 3, 7, 2, 9, 4];
```

# Q2 - Find Peak

In an array, a peak is when a number is greater than all of its neighbors.

```javascript
var ar = [1, 5, 3, 7, 8, 11, 9, 10]
```

In the above array, the peaks are:  
5, 11, and 10

Write a code that will find any one peak from an array.
