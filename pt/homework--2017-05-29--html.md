# HTML/CSS Homework

## Q1
Create the following table using HTML/CSS

<img src="https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fimg%2Fauthor_table.png?alt=media&token=abd6a3ef-c314-473b-a365-b9d26745df83">


## Q2

Given the following HTML, determine how many `<div>` tags are affected by the following CSS statements

```html
<div class="box1">
    <div>1</div>
    <div>2</div>
    <div>3</div>
    <div class="special">4</div>
    <div>5</div>
</div>
<div class="box2">
    <div>1</div>
    <div>2</div>
    <div class="special">3</div>
    <div>4</div>
    <div>5</div>
</div>
```

**CSS**

```css
div {}

div.box1 {}

.box1 div {}

.box2 > div {}

.special + div {}

.special ~ div {}
```

# Solutions

## Q1

```html
<!-- HTML -->
  <table>
     <tr>
       <th>Author</th>
       <th>Genre/Title</th>
    </tr>
    
    <tr>
      <td>A. Christie</td>
      <td>Miss Marple</td>
    </tr>
    
    <tr class="highlight">
      <td>W. Shakespeare</td>
      <td>Plays</td>
    </tr>

     <tr>
       <td>S. Sheldon</td>
       <td>Suspense</td>
    </tr>
    
    <tr>
      <td>J.K. Rowling</td>
      <td>Harry Potter</td>
    </tr>
  </table>
```

```css
/* CSS */
table {
  border-collapse: collapse;
}

th {
  border: 1px solid black;
  padding: 10px;
  background-color: darkblue;
  color: white;
}

td {
  border: 1px solid black;
  padding: 10px;
}

.highlight {
  background-color: skyblue;
}
```

## Q2

```language-none
10
1
4
4
2
3
```