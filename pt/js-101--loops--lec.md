# JS 101 - Loops

**FOR** and **WHILE**

---

# WHILE

```js
while(true)
{
    console.log("1");
}
```

<div class="ui negative message">Warning. Do NOT run this</div>

---

# WHILE (2)

**Simple Repeater**

```js
var i = 0;

while ( i < 10 ) 
{
    console.log("hello");
    i++;
}
```

**Simple Counter**

```js
var i = 0;

while ( i < 10 ) 
{
    console.log(i);
    i++;
}
```

---

## Quiz

Print the following:

```language-none
10
20
30
40
50
```

```language-none
2
4
6
8
10
```

---

# WHILE (3)

**Reverse**
```js
var i = 5;
while(i >= 0) 
{
    console.log(i)
    i--
}
```

QUIZ: print numbers 10 ~ 0.

---

# WHILE (4)

**Combination with IF**
```js
var i = 1

while(i <= 5) 
{
  if (i == 3) 
  {
    console.log("three!")
  } 
  else 
  {
    console.log(i)  
  }
  
  i++
}
```

QUIZ: print all **even** numbers between 0 and 20.

---

# FOR

**WHILE**
```js
var i = 0;
while( i < 10 ) 
{
    console.log(i);
    i++;
}
```

**FOR**
```js
for ( var i = 0; i < 10; i++ ) 
{
    console.log(i);
}
```

--- 

## QUIZ

1. Print `hello` 10 times
2. Print numbers 1 ~ 10
3. Print numbers 10 ~ 1
4. Print all `even` numbers between 10 and 20

---

# FOR (2)

3 ways to print

```language-none
2
4
6
8
10
```

---

## Answer

```js
for(var i = 2; i <= 10; i = i + 2) {
    console.log(i);
}

for(var i = 1; i <= 5; i++) {
    console.log( i * 2 );
}

for(var i = 1; i <= 10; i++ ) {
    if ( i % 2 == 0 ) {
        console.log(i);
    }
}
```

---

# LOOP CONTROL

`continue` and `break`

---

# CONTINUE

Skips the remaining code block

```js
for( var i = 0; i < 5; i++ ) {
    if (i == 2) {
        continue;
    }
    console.log(i);
}
```

---

# BREAK

End the loop

```js
for( var i = 0; i < 5; i++ ) {
    if (i == 2) {
        break;
    }
    console.log(i);
}
```

---

## Quiz

Solve the following questions using either `continue` or `break`

1. Print 1 ~ 10 but exclude 2
2. Print all numbers from 100 to the first number that is divisible by 13.

---

# MORE QUIZZES

Q.
```language-none
*****
```

Q.
```language-none
*
**
***
****
*****
```

Q. Print all numbers between 0 and 100 that contains 3 (3, 13, 23, 30, 31, ... 93)

---

# More Quizzes

Output?

```js
for(var i=0; i<3; i++) {
  
  for(var j=0; j<3; j++) {
    console.log(i, j);
  }

}
```

```js
for(var i=0; i<3; i++) {

  for(var j=i; j<3; j++) {
    console.log(i, j);
  }

}
```

```js
for(var i=0; i<3; i++) {

  for(var j=3-i; j>=0; j--) {
    console.log(i, j);
  }

}
```