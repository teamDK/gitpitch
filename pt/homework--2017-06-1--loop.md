# Loop

## Q1

Print all multiples of threes between 0 & 100

```language-none
3
6
9
12
.
.
.
99
```

## Q2

Print 1 through 10 but *exclude* multiples of 4


## Q3

Print 1 ~ 10 squared.

```language-none
1
4
9
.
.
.
81
100
```

# Solution

```js
// Q1
for(var i=0; i<100; i+=3) {
    console.log(i);
}

// Q2
for(var i=1; i<=10; i++) {
    if (i % 4 != 0) {
        console.log(i);
    }
}

// Q3
for(var i=1; i<=10; i++) {
    console.log(i * i);
}
```
