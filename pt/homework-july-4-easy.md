# Easy

With answer at the bottom!

### Q1

Print 1 ~ 20.
Then print 20 ~ 1.

### Q2

Print 1 ~ 20, but exclude 10

### Q3

Print 1 ~ 20, exluding 5 and 10

### Q4

Print 2 ~ 20, but only the even numbers

### Q4

Print 1 ~ 10. Specify oddify for each number.

```language-none
1 odd
2 even
3 odd
4 even
5 odd
6 even
7 odd
8 even
9 odd
10 even
```

# Answers

### Q1

```js
var i=1
while(i<=20) {
  console.log(i)
  i++
}
```

or using the for loop

```js
for(var i=1; i<=20; i++) {
  console.log(i)
}
```

### Q2

```js
for(var i=1; i<=20; i++) {
  if (i != 10) {
    console.log(i)
  }
}
```

### Q3

```js
for(var i=1; i<=20; i++) {
  if (i != 5 && i != 10) {
    console.log(i)
  }
}
```

### Q4

```js
for(var i=2; i<=20; i = i + 2) {
    console.log(i)
}
```

### Q5

```js
for(var i=1; i<=10; i++) {
    if (i % 2 == 0) {
      console.log(i + " even")
    } else {
      console.log(i + " odd")
    }
}
```