# Javascript 101

## Basic

---

# Print to console

```js
console.log("hello world")
```

---

# Variables

```js
var a = 10
console.log(a)

a = 20
console.log(a)
```

---

# Variables

**Number**
```js
var a = 10
var b = 3.14
```

**String**
```js
var c = 'hello'
var d = "hi"
```

**boolean**
```js
var e = true
var f = false
```

---

# Operators (String)

String only has 1 operator `+`

```js
var str1 = 'hello'
var str2 = 'world'
console.log(a + ' ' + b);
```

--- 

# Operators (Numbers)

## 1. Arithmetic

```js
var x = 1 + 2
var y = 3 - 2
var z = x * y

var a = 10 / 3
var b = 10 % 3
```

Order of precedence

```js
console.log( 1 + 2 * 3 );
```

---

## Quiz

```js
var a = (1 + 2) * (4 - 2 / 0.5)
console.log(a)

var b = 10 * 5 % 2
console.log(b)

var c = 1 + 2 * (3 + 1) / 2
console.log(c)

var d = 8 % 2 + 9 % 2
console.log(d)

var e = 123 % 10
console.log(e)
```

---

# Operators (Numbers)

## 2. Assignment

```js
var x = 1

x += 2
x -= 2
x *= 2
x /= 2
x %= 2
```

**Advanced**

```js
x++
x--

++x
--x
```

---

## Quiz

```js
var a = 10

a += 2
console.log(a)

a -= 1
console.log(a)

a *= 2
console.log(a)

a /= 2
console.log(a)

a %= 2
console.log(a)

a -= a
console.log(a)
```

---

## Quiz (Advanced)

```js
var a = 10

var b = a++

var c = ++a

var d = (a++) + (++a)
```

---

# Comparison Operators

yields a `boolean`

| operator | description |
|---|---|
| == |	equal to |
| === |	equal value and equal type |
| != |	not equal |
| !== |	not equal value or not equal type |
| > |	greater than |
| < |	less than |
| >= |	greater than or equal to |
| <= |	less than or equal to |
| ? |	ternary operator |

---

## Examples

```js
2 > 3   // false
2 >= 2  // true
10 != 9 // true

2 == 2  // true
2 === 2 // true

2 == "2"    // true
2 === "2"   // false
```

---

# Logical Operators

| operator | description |
|---|---|
| && | Logical AND |
| &#124;&#124; | Logical OR |
| ! | Logical NOT |

---

## Examples

```js
var a = true;
var b = !false;
var c = !!true;
var d = true && true;
var e = ( false && true ) || true;
var f = (1 > 0) && (3 <= 12);
var g = (true == true);
```

---

## Quiz

Given

```js
var x = 10
```

Determine whether each variables is `true`, `false` or error

```js
var a = 1 < x

var b = x <= 20

var c = 1 < x < 20

var d = x > 1 || x < 10

var e = x == 10 || 20

var f = (x = 10)

var g =  x < 1 && x > 20
```