# JS 101 - Objects

---

# Creating a object

```js
var person = {
    name: 'Luke',
    age: 10
}
```

```js
console.log(person);
```

---

# Object Properties

```js
var person = {
    name: 'Luke',
    age: 10
}

person.age = 22;
person.name = 'Anakin'

console.log(person.age);
console.log(person.name);
```

---

# Object Functions

```js
var person = {
    name: 'Luke',
    age: 10,
    attack: function() {
        console.log("zoom");
    }
}

person.attack();
```

---

# Object Functions

```js
var x = 1;

var person = {
    name: 'Luke',
    age: 10,
    attack: function() {
        console.log(x);
    },
    x: 2
}

person.attack();
```

---

# 'this' keyword

```js
var x = 1;

var person = {
    name: 'Luke',
    age: 10,
    attack: function() {
        console.log(this.x);
    },
    x: 2
}

person.attack();
```

---

# Multiple Objects

```js
var hero = {
    hp: 100,
    pow: 10,
}

var monster = {
    hp: 500,
    pow: 20,
}

function fight(h, m) {
    console.log("hero: " + h.hp, "monster: " + m.hp);
    h.hp -= m.pow;
    m.hp -= h.pow;
}

while (hero.hp > 0 && monster.hp > 0) {
    fight(hero, monster);
}
```

---

# Array of objects

```js
var ar = [
    { name: 'luke', power: 70 },
    { name: 'anakin', power: 80 },
    { name: 'yoda', power: 90 },
    { name: 'han', power: 60 }
]

for (var i = 0; i <= ar.length - 1; i++) {
    console.log(ar[i].name);
}
```

Quiz: Print names of those whose power is greater than 75

---

# Quiz

Complete the `car` object

```js
var car = {
    // fill in
}

car.speed = 10;

for (var i = 0; i < 5; i++) {
    car.run();
}
```

**expected output**

```language-none
car is at x = 10
car is at x = 20
car is at x = 30
car is at x = 40
car is at x = 50
```