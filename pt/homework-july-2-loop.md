### Q1

Print 1 through 20

### Q2

Print 1 through 20 except for 10

### Q3

Print all even numbers between 0 and 10

### Q4

Print all multiples of 13 between 0 and 100

### Q5

Print the following

```language-none
1
11
2
22
3
33
4
44
5
55
6
66
7
77
8
88
9
99
```

### Q6

Print 15 asterisks(*) in one line