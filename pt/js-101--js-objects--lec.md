# JS 101 - JS Objects

Some essential javascript functions

---

# Math

```js
Math.random()
Math.floor(12.5)
Math.pow(2, 3)
Math.PI
Math.sqrt(16)
```

## Quiz 

- Generate a random number between 0 and 2

- Generate a random number between 2 and 5

---

# String

```js
'hello'.length
'world'.charAt(0)

// These functions do NOT alter the original string but return new ones.
'radio'.replace('a','o')
'rainbow'.substring(1)
'rainbow'.substring(1, 3)
```

## Example 

```js
var str = 'radio'
var str2 = str.replace('a','o').substring(1)
console.log(str, str2);
```

---

## Quiz

Determine the values of each variable

```js
var str = 'blue sky'

var str2 = str.replace('blue', 'red').replace(' ', ',')

var str3 = str.replace('sky', '!').substring(1, 3)

var str4 = str.replace(' sky', '').charAt(3)
```

---

# Array

```js
var ar = [1, 3, 7, 2, 9]

ar.sort()
ar.indexOf(3)
ar.push(5)
ar.pop()
ar.splice(3)
ar.splice(1, 1)
```

All of these functions alter the original array

---

## Quiz

Discuss the array content after each line

```js
var ar = [1, 3, 7, 2, 9]

ar.push(3)
ar.pop()
ar.splice(3)
ar.push(1)
ar.sort()
ar.splice(2, 1)
ar.push(ar[0])
ar.push(ar.pop())
ar.push(ar.indexOf(7))
```


