# WEB 101

- HTML
- CSS
- JS

---
# Web Page

**html**  
Structure, Content
```html
<p>This is a paragraph</p>
<em>This text is emphasized</em>
```

**css**  
Appearance, Format
```css
* {
	color: red;
	width: 20px;
}
```

**javascript**  
Functionality
```js
if (button.clicked) {
	alert("Thank you!");
}
```

---

# HTML

Hypertext Markup Language

Editors
- [Visual Studio Code](https://code.visualstudio.com/) - recommended
- [Atom](https://atom.io)

Online Editors
- [jsbin](https://jsbin.com)
- [jsFiddle](https://jsfiddle.net)
- [plunkr](https://plnkr.co/)

Cheatsheet
- [download](https://drive.google.com/open?id=1tF8Li20Yqx09NZBOr0jxvT5rbHWn4G9OO4YQ_kQVvdw)

---

# Table

<table border=1>
	<tr>
		<th>header1</th>
		<th>header2</th>
	</tr>
	<tr>
		<td>1</td>
		<td rowspan=2>2</td>
	</tr>
	<tr>
		<td>1</td>
	</tr>
	<tr>
		<td colspan=2>1</td>
	</tr>
	<tr>
		<td>1</td>
		<td>2</td>
	</tr>
</table>

```html
<table border=1>
	<tr>
		<th>header1</th>
		<th>header2</th>
	</tr>
	<tr>
		<td>1</td>
		<td rowspan=2>2</td>
	</tr>
	<tr>
		<td>1</td>
	</tr>
	<tr>
		<td colspan = 2>1</td>
	</tr>
	<tr>
		<td>1</td>
		<td>2</td>
	</tr>
</table>
```

---

# Forms

<form>
	<fieldset>
		<legend>personal</legend>
		<label for="first-name">First name</label>
		<input type="text" id="first-name">
		<label for="last-name">Last name</label>
		<input type="text" id="last-name">
	</fieldset>
	<fieldset>
		<legend>optional</legend>
		<label for="nick-name">Nick name</label>
		<input type="text" id="nick-name">
	</fieldset>
</form>

```html
<form>
	<fieldset>
		<legend>personal</legend>
		<label for="first-name">First name</label>
		<input type="text" id="first-name">
		<label for="last-name">Last name</label>
		<input type="text" id="last-name">
	</fieldset>
	<fieldset>
		<legend>optional</legend>
		<label for="nick-name">Nick name</label>
		<input type="text" id="nick-name">
	</fieldset>
</form>
```

---

# Label

```html
<input type="checkbox" id="option1">Option 1

<input type="checkbox" id="option2"><label for="option2">option2</label>
```

---

# Exercise 1

<h3>Standings</h3>
<table>
	<tr>
		<th>Name</th>
		<th>W</th>
		<th>L</th>
	</tr>
	<tr>
		<td>Bulls</td><td>10</td><td>2</td>
	</tr>
	<tr>
		<td>Lakers</td><td>8</td><td>5</td>
	</tr>
	<tr>
		<td>Kings</td><td>3</td><td>6</td>
	</tr>
	<tr>
		<td>Suns</td><td>1</td><td>9</td>
	</tr>
</table>

---

## Answer

```html
<h3>Standings</h3>
<table>
	<tr>
		<th>Name</th>
		<th>W</th>
		<th>L</th>
	</tr>
	<tr>
		<td>Bulls</td><td>10</td><td>2</td>
	</tr>
	<tr>
		<td>Lakers</td><td>8</td><td>5</td>
	</tr>
	<tr>
		<td>Kings</td><td>3</td><td>6</td>
	</tr>
	<tr>
		<td>Suns</td><td>1</td><td>9</td>
	</tr>
</table>
```

---

# Exercise 2

image address: http://lorempixel.com/400/200/

<img src="http://lorempixel.com/400/200/" height="100px">
<p>Oh, say<b>!</b> can you see </p>
<p>by the dawn's early <em>light</em></p>

---
## Answer

```html
<img src="http://lorempixel.com/400/200/">
<p>Oh, say<b>!</b> can you see </p>
<p>by the dawn's early <em>light</em></p>
```

---

# Exercise 3

<img src="http://i.imgur.com/jOLfqgD.png" />

---

## Answer

```html
<h4>Login</h4>

<form>
	<label for="username">Username</label>
	<input type="text" name="username" size="10">
	
	<label for="password">Password</label>
	<input type="text" name="password" size="10">
	
	<input type="button" value="CLICK">
</form>
```