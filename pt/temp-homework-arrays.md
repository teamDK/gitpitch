# Javascript Homework

## Q1

Print the following:

```language-none
0-1-2-3-4-5-6-7-8-9
```

## Q2

Print all numbers that are

- Between 0 and 100
- Contains 3

```language-none
3
13
23
30
31
32
33
34
35
36
37
38
39
43
53
63
73
83
93
```

## Q3.

Given an array of negative numbers, find the maximum.

```javascript
var ar = [-1, -9, -11, -20, -7]
```

## Q4.

Print the names of students who got an `A` in at least one subject

```javascript
var ar = [
    { name: "Johnny", math: 'A', science: 'B' },
    { name: "Mike", math: 'B', science: 'C' },
    { name: "Donny", math: 'B', science: 'B' },
    { name: "Zane", math: 'C', science: 'A' },
]
```

## Q5.

Write a function that returns the maximum of 3 numbers

```javascript
function max(a, b, c) {

}
```
  
    
      

# Answers



```javascript
// Q1
var line = ''
for (var i = 0; i < 10; i++) {
    line += i

    if (i < 9) {
        line += "-"
    }
}
console.log(line);

// Q2
for (var i = 0; i <= 100; i++) {
    if (i % 10 == 3) {
        console.log(i)
    }
    else if (i >= 30 && i <= 39) {
        console.log(i);
    }
}

// Q3
var ar = [-1, -9, -11, -20, -7]

var max = ar[0]
for (var i = 0; i < ar.length; i++) {
    if (max < ar[i]) {
        max = ar[i]
    }
}

console.log("Max is " + max);

// Q4.
var ar = [
    { name: "Johnny", math: 'A', science: 'B' },
    { name: "Mike", math: 'B', science: 'C' },
    { name: "Donny", math: 'B', science: 'B' },
    { name: "Zane", math: 'C', science: 'A' },
]

for (var i = 0; i < ar.length; i++) {
    var student = ar[i];
    if (student.math == 'A' || student.science == 'A') {
        console.log(student.name);
    }
}

// Q5.
function max(a, b, c) {
    if (a > b && a > c) {
        return a;
    }
    else if (b > c) {
        return b;
    }
    else {
        return c;
    }
}
```