# Array Homework Quizzes

### Q1.

Print only the numbers that are in an ascending order.

```js
var ar = [1, 3, 7, 9, 11, 5, 13]
```

### Q2.

Count the number of even and odd numbers from the array. Use a loop (for / while) only once.

```js
var ar = [1, 19, 20, 11, 4, 2, 9]
```

### Q3

Find the second *smallest* number

```js
var ar = [12, 456, 213, 431, 14361, 653341, 41, 6, 89, 2314, ]
```

### Q4

Given these two arrays, print all numbers in an ascending order

```js
var ar1 = [1, 3, 5, 7, 9]
var ar2 = [2, 4, 6, 8, 10]
```

### Q5 (hard)

Given these two arrays, print all numbers in an ascending order

```js
var ar1 = [1, 3, 4, 7, 9]
var ar2 = [2, 5, 6, 8, 10]
```
