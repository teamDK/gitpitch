Q1. Review lecture slides

Q2. Determine the value of each variable below. Run the code to verify your answer.

```js
var a = 1 + 3 * 10;
var b = 3 / 2 + 1;
var c = 10 - ( 2 * 2 );
var d = 3 % 2;
var e = 10;
e = e * 2;
e += 2;

var f = 1 == 1;
var g = 2 < 10;
var h = 4 === "4";

console.log(a,b,c,d,e,f,g,h);
```

Q3. Calculate the outputs without running the codes then verify.

```js
console.log( 2931820312 % 2 )

console.log( 122 % 10 + 122 % 100 )
```