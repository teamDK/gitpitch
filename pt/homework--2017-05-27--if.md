# If / Switch Questions

## Q1

Given a `score` variable, print their grades according to the following table. Use a `if` statement.

| score | grade |
|---|---|
| 90-100 | EC |
| 50-89 | P |
| 0-49 | F |

## Q2

Two boolean variables `math` and `english` represent whether you have passed the respective subject. Print:

- 'A' if passed both OR
- 'B' if passed only one subject OR
- 'C' if passed neither.

## Q3

Determine the value of each variables after the following code snippet. Verify using actual codes.

```js
var a = 10
var b = a++
var c = (++a) - (++b)
var d = a % 2 + b / 2
```

## Q4

Determine the output of the following code snippet. Verify using actual codes

```js
var a = 104

switch(a % 5) {
    case 1:
        consol.log("I am the first!");
        break;
    case 2:
    case 3:
        console.log("Hello");
    case 4:
        break;
    case 5:
        console.log("Uh Oh");
    default:
        break;
}
```

# Solutions

## Q1

```js
var score = 50;

if (score >= 90)
{
  console.log("EC");
}
else if (score >= 50) 
{
  console.log("P");  
}
else
{
  console.log("F");
}
```

## Q2

```js
var math = true
var english = false

if (math && english) 
{
  console.log("A")
}
else if (math || english)
{
  console.log("B")
}
else
{
  console.log("C")
}
```