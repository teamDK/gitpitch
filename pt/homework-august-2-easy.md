## Q1.

Calculate

```language-none
1 + 2 + 3 + ... + 97 + 98 + 99
```

## Q2.

Calculate

```language-none
1 + 2 - 3 + 4 - 5 + 6 - 7 .... + 98 - 99 + 100
```

## Q3.

Print 1 ~ 20. For even numbers, just print `even`

```language-none
1
even
3
even
5
even
...
...
```
