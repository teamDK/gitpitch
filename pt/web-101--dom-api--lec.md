# DOM

DOM is a programming interface for HTML, XML and SVG documents. It provides a structured representation of the document as a tree. The DOM defines methods that allow access to the tree, so that they can change the document structure, style and content.DOM is a programming interface for HTML, XML and SVG documents. It provides a structured representation of the document as a tree. The DOM defines methods that allow access to the tree, so that they can change the document structure, style and content.

---

# DOM interfaces

```
Attr
CharacterData
ChildNode
Comment
CustomEvent
Document
DocumentFragment
DocumentType
DOMError
DOMException
DOMImplementation
DOMString
DOMTimeStamp
DOMSettableTokenList
DOMStringList
DOMTokenList
Element
Event
EventTarget
HTMLCollection
MutationObserver
MutationRecord
Node
NodeFilter
NodeIterator
NodeList
ParentNode
ProcessingInstruction
Range
Text
TreeWalker
URL
Window
Worker
XMLDocument
```

---

# DOM interfaces

- Document
- Element
- Event
- Node
- Window

---

# Window

```js
window.alert()
window.confirm()
window.document
window.location
```

<a href="https://developer.mozilla.org/en-US/docs/Web/API/Window">Complete List</a>

---

# DOCUMENT

A direct child of `window`.

```js
document.addEventListener()
document.body
document.createAttribute()
document.createElement()
document.createTextNode()
document.documentURI
document.forms
document.getElementById()
document.getElementsByClassName()
document.getElementsByName()
document.getElementsByTagName()
document.head
document.querySelector()
document.querySelectorAll()
document.removeEventListener()
document.title
document.URL
document.write()
document.writeln()
```

Example

```js
    var heading = document.createElement("h1");
    heading.innerHTML = "hello";
    document.body.appendChild(heading);
```

---

# Node

In the HTML DOM, everything is a node

-document itself
-HTML elements (element node)
-HTML attributes (attribute node)
-Text content (text node)
-Comments (comment node)

---

#  Element

Element object is one type of a Node object which represents an HTML element.

Element objects can have child nodes (element nodes, text nodes, comment nodes)

The DOM consists of a hierarchy of nodes where each node can have a parent, a list of child nodes and a nextSibling and previousSibling. That structure forms a tree-like hierarchy. The document node would have its list of child nodes (the head node and the body node). The body node would have its list of child nodes (the top level elements in your HTML page) and so on.

```js
element.addEventListener()
element.appendChild()
element.attributes
element.blur()
element.childElementCount
element.childNodes      // all child nodes (text, comment nodes)
element.children        // child elements
element.classList
element.className
element.click()
element.cloneNode()
element.contains()
element.firstChild
element.focus()
element.getAttribute()
element.getAttributeNode()
element.hasAttribute()
element.hasAttributes()
element.id
element.innerHTML
element.insertBefore()
element.isEqualNode()
element.isSameNode()
element.lastChild
element.nextSibling
element.nodeType
element.parentNode
element.parentElement
element.previousSibling
element.querySelector()
element.querySelectorAll()
element.removeAttribute()
element.removeAttributeNode()
element.removeChild()
element.replaceChild()
element.removeEventListener()
element.style
element.toString()

nodelist.item()
nodelist.length
```

---

# Attribute nodes

```js
attr.name
attr.value
```

---

# Examples

Creating new nodes

```js
    var heading = document.createElement("h1");
    var heading_text = document.createTextNode("Big Head!");
    heading.appendChild(heading_text);
    document.body.appendChild(heading);
```

child list

```html
  <a href="google.com">
    visit <em>google</em>
  </a>
```

```js
var aTag = document.querySelector("a");

console.log(aTag.childNodes)
console.log(aTag.children)

console.log(aTag.getAttribute('href'))
console.log(aTag.getAttributeNode('href'))
```

---

# Events

```js
// mouse
onclick
oncontextmenu
ondblclick
onmousedown
onmouseenter
onmouseleave
onmousemove
onmouseover
onmouseout
onmouseup

// keyboard
onkeydown
onkeypress
onkeyup

// form
onblur
onchange
onfocus
onfocusin
onfocusout
oninput
oninvalid
onreset
onsearch
onselect
onsubmit
```

---

# Registering event listeners

```js
btn.addEventListener('click', function() { ... }, false);
```

```js
btn.onclick = function() { ... };
```

```html
<button onclick="...">
```