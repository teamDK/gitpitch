# CSS Layout

- display, margin, float, width, position, flex

---

# Display

- block
- inline
- inline-block
- none
- flex*

---

# margin:auto

```css
.centered {
    width: 300px;
    margin: 0 auto;
}
```

---

# width

```css
div {
    width: 500px;
    min-width: 400px;
    max-width: 600px;
}
```

---

# Box Sizing

- default: content
- box-sizing: content + padding + border

```css
#box1 {
    width: 500px;
    margin: 20px auto;
    border: 5px solid skyblue;
    padding: 0;

    box-sizing: box-sizing;
}
```

```css
#box2 {
    width: 500px;
    margin: 20px auto;
    border: 10px solid skyblue;
    padding: 10px;

    box-sizing: box-sizing;
}
```

---

# Position

| position | desc |
|--|--|
| static | Not positioned |
| relative | Relative to the original (static) position |
| absolute | Relative to the **nearest positioned ancestor** |
| fixed | Relative to the viewport |

---

# Float

```css
img {
    float: left;
}

img {
    float: right;
}

.clear {
    clear: both;
    clear: left;
    clear: right;
}
```

---

# Units

| Unit | Desc |
|--|--|
| px | Pixels. |
| pt | Points. 1 pt = 1/72 inch. |
| em | Scalable unit. 1em = the font-size of the document in pt |
| % | 100% = the current font size |

---

# Column

```css
p {
    column-count: 3;
    column-gap: 1em;
}
```

---

# Exercise

<img src="https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fimg%2Fweb-101--css2--lec.jpg?alt=media&token=770c1872-77e0-46b1-acff-388b338a58c3">