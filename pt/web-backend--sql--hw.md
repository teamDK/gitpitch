Do the following exercise in your own database.

For each question, use an SQL statement. ( i.e. Do not rely on GUI )

**Q1.** Create a `student` table with following columns

- `id` is an auto-generated numerical primary key.
- `name` field contains strings with a maxlength of 100
- `year` is a number between 6 - 12
- `avg` is a floating point number between 0 - 100

**Q2.** Populate the `student` table with the following data

| id | name | year | avg |
|--|--|--|--|
| 1 | Daniel | 11 | 90.5 |
| 2 | John | 10 | 80.2 |
| 3 | Sue | 12 | 100 |
| 4 | Alex | 12 | 94 |
| 5 | Donald | 10 | 90 |
| 6 | Joan | 9 | 88 |

Q3 ~ Q10 Write an SQL statements that

- **Q3.** Selects the top 5 students based on average score.
- **Q4.** Counts the number of students in year 12
- **Q5.** Counts the number of year 10 students whose average score is 90 or better
- **Q6.** Calculates the average score of all year 10 students
- **Q7.** Calculates the average score of all students whose individual average is better than 90.
- **Q8.** Calculates the average score of all students in each grade
- **Q9.** Figures out which year averaged the highest
- **Q10.** Gives an extra 5 points to the average for those who score less than 90

**Q11.** Using 1 SQL statement, give everyone a random average between 0 and 100.

**Q12.** Add a column named `pass` which will store a boolean

**Q13.** Fill the `pass` column with true and false. Those who score 50 or better shall get a pass.




