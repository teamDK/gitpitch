# SQL

---

# Database

```sql
CREATE DATABASE test;

SHOW DATABASES;

USE test;

DROP DATABASE test;
```

---

# Data Types

|  | Data Type |
|--|--|
| Num - Integral | INT, TINYINT, SMALLINT, MEDIUMINT, BIGINT |
| Num - Floating | FLOAT(M,D), DOUBLE(M,D), DECIMAL(M,D) |
| Date, Time | DATE, DATETIME, TIMESTAMP, TIME, YEAR |
| String | CHAR, VARCHAR, ENUM |
| Text |  BLOB / TEXT, TINYTEXT, MEDIUMTEXT, LONGTEXT |

---

# Create Table

```sql
SHOW TABLES;

CREATE TABLE table_name (
    col_name col_type <constraints>,
    col_name col_type,
    col_name col_type,
    ...,
    PRIMARY KEY(col_name, col_name),
    FOREIGN KEY(col_name) REFERENCES table_name(col_name)
)
```

```sql
CREATE TABLE student (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fname VARCHAR(50) NOT NULL,
    lname VARCHAR(50) NOT NULL,
    birthdate DATE,
    student_id VARCHAR(50) NOT NULL UNIQUE,
)
```

---

# Constraints

- PRIMARY KEY
- UNIQUE
- NOT NULL
- DEFAULT
- AUTO_INCREMENT
- COMMENT
- KEY | INDEX
- FOREIGN KEY

---

# Alter, Drop Table

```sql
ALTER TABLE table_name
    ADD COLUMN col_name col_type <constraints>
    DROP COLUMN col_name
    CHANGE col_name col_new_name col_type
    RENAME COLUMN col_name TO col_new_name

DROP TABLE table_name
```

```sql
ALTER TABLE student
    ADD COLUMN score DATE AFTER student_id
```

---

# INSERT

```sql
INSERT INTO table_name ( field1, field2,...fieldN )
                       VALUES
                       ( value1, value2,...valueN );
```

```sql
INSERT INTO student ()
values
('a@a.com', 'ABC', CURDATE()),
('b@a.com', 'BBQ', NOW());
```

---

# SELECT

```sql
SELECT field1, field2,...fieldN table_name1, table_name2...
[WHERE Clause]
[OFFSET M ][LIMIT N]
```

field name can be escaped

```sql
select `name` from `table`
```

---

# WHERE

## Numbers

- =
- !=
- &gt;
- &lt;
- &gt;=
- &lt;=

- AND
- OR
- (not) In

- IS NULL / IS NOT NULL

## String 

- =, != (case insensitive)
- BINARY = (case sensitive)
- LIKE

```sql
SELECT * FROM student
WHERE BINARY email = 'irvinecode@gmail.com';

SELECT * FROM student
WHERE email LIKE '%@gmail.%';

SELECT * FROM student
WHERE id > 10 AND id < 20
```

---

# UPDATE/ DELETE

```sql
UPDATE table_name SET field1=new-value1, field2=new-value2
[WHERE Clause]

DELETE FROM table_name [WHERE Clause]
```

Q. Update all user score to be `1`
Q. Update all user score to be `id * 10`
Q. Update all user score to be random

---

# Advanced SELECT

```sql
SELECT columns
FROM table
WHERE condition

GROUP BY columns
HAVING conditions
ORDER BY columns
```

---

## Quiz

- Select all students whose score is greater than 50
- Select top 6 students
- Calculate avg of all students
- Calculate the number of students who passed
- Calculate the avg of students based on their reg-date
- Select reg-date groups whose avg score is better than 50

---

# Relationship

## 1 : 1

user - student

## 1 : N

student - favorite color

## N : N

friends (student - student)


---

# Foreign Key

```sql
CREATE TABLE Teacher (
    teacher_id int auto_increment,
    user_id int,
    PRIMARY KEY (teacher_id),
    FOREIGN KEY (user_id) REFERENCES user(user_id)
);
```

---

# 1: 1

User - uid
Teacher - tid, (uid)

Q. Why not have `tid` in User

Q. Why not both?

---

# 1 : N

Teacher (1) - tid
Class (N) - cid, (tid)

---

# Fetching from Multiple tables

```sql
select Teacher.*, User.firstname from User, Teacher
where user.id = score.id
```

---

# JOINS

- Self Join
- Inner Join
- Outer Join
- Left Join
- Right Join

```sql
<join_type> ::= 
    [ { INNER | { { LEFT | RIGHT | FULL } [ OUTER ] } } [ <join_hint> ] ]
    JOIN
```

---

# JOINS

<img src="https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fimg%2FVisual_SQL_JOINS_V2.png?alt=media&token=20020d90-0605-40ab-9ffd-8340be81f5d8">

---

# Quiz

```language-none
A    B
-    -
1    3
2    4
3    5
4    6
```

```sql
/* inner join */
select * from a INNER JOIN b on a.a = b.b;
select a.*,b.*  from a,b where a.a = b.b;

/* Left Outer Join */
select * from a LEFT OUTER JOIN b on a.a = b.b;

/* Right outer join */
select * from a RIGHT OUTER JOIN b on a.a = b.b;

/* Full outer join */
select * from a FULL OUTER JOIN b on a.a = b.b;
```