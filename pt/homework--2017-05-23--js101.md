# JS 101 Homework (2017/5/23)


## Q1

Start from the number `100`, keep dividing by 2 and print along until the number reaches 1.

hint: In theory, diving a number by 2 will never make it 0. So make sure to stop at 1 not 0!

```javascript
100
50
25
12.5
6.25
3.125
1.5625
```

## Q2

```js
var ar = [1, 7, 10, 13, 19, 22];
```

Check if the number `19` exists in an array.

## Q3

Write a function that finds all divisors for an integer. (Exclude 1 and itself)

```js
divisor(10);    // returns 2, 5
divisor(20);    // returns 2, 4, 5, 10
divisor(3); // returns 
divisor(30); // returns 2, 3, 5, 6, 10, 15
```

extra credit: Find ONLY the first ~2 divisors.

## Q4

Given an array, find the median.

(median:  the "middle" value in the list of numbers.)

```js
var ar = [1, 10, 3, 7, 4]
// median is 3
```

# Answers

## Q1

```js
ar num = 100;
while(num >= 1) {
  num /= 2;
  console.log(num);
}
```

## Q2

```js
var ar = [1, 7, 10, 13, 19, 22];

for (var i = 0; i <= ar.length - 1; i++) {
    if (ar[i] == 19) {
        console.log("19 exists")
    }
}
```

## Q3

```js
function divisor(num) {
    for (var i = 2; i <= num / 2; i++) {
        if (num % i == 0) {
            console.log(i + " is a divisor");
        }
    }
}

divisor(10);
divisor(20);
divisor(3);
divisor(30);
```

extra credit:

```js
function divisor(num) {
    var found = 0;

    for (var i = 2; i <= num / 2; i++) {
        if (num % i == 0) {
            console.log(i + " is a divisor");
            found++;
            if (found >= 2) {
                break;
            }
        }
    }
}
```

Q4

```js
var ar = [1, 10, 3, 7, 4]

ar.sort();

var mid = Math.floor(ar.length / 2)

console.log(ar[mid])
```