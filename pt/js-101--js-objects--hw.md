# JS 101 - JS Objects - Homework

## Q1. 

What are the possible values of x ? List them all.

```js
var x = Math.random() * 2;
x = Math.floor(x);
```

## Q2. 

What is the value of x ?

```js
var x = 'united states of america'.substring(0, 2)
```

## Q3. 

Write a code that will generate an integer between 0 and 5 (inclusive)

## Q4.

Write a code that will generate an integer between 2 and 7 (inclusive)

## Q5. 

A movie name is misspelled. Correct it using a `replace()` function.

```js
var x = 'dark night'
```

## Q6.

Write a function that will print the array's **min** and **max** values without using a loop (i.e. no `for` or `while` loops).

hint: `sort()`

```js
minmax([3, 2, 7, 1, 4]);
minmax([9, 4, 2, 1, 3]);
```

**expected output**
```language-none
1, 7
1, 9
```