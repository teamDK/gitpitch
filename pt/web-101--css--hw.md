[1. CSS Basics](https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fcss-quiz-1.html?alt=media&token=42698050-5d11-4f65-98b6-4fbb68f09739)

[2. CSS Layouts](https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fcss-quiz-2.html?alt=media&token=e05eb31e-4bfa-40bb-9afe-305aabe8ceb1)

[3. CSS Selectors](https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fcss-quiz-3.html?alt=media&token=8baf7144-06ad-4fbf-ba89-38c291e92e8d)
