# Functions

Inputs ---> [FUNCTION] ---> Output

---

# Syntax

```js
function name(parameters) {
    // function body
    return retVal;
}
```

---

# Parameters

```js
function fn1() {
    console.log(10)
}
```

```js
function fn2(a) {
    console.log(a);
}
```

```js
function fn3(a, b) {
    console.log(a + b);
}
```

---

## Quiz

`sum()` prints the sum of 2 number parameters

```js
sum(10, 5);
```

`max()` prints the bigger of the 2 number parameters

```js
max(10, 5);
max(5, 10);
```

---

# Return

```js
function add(a, b) {
    return a + b;
}

var sum = add(10, 5);
```

---

## Quiz

```js
function add(a, b) {
    return a + b;
}

var x = add(10, 5) + add(2, 4);

var y = add(add(10, 2), 2);

var z = add(add(1, 1), add(2, 2));

console.log(x, y, z);
```

---

# JAVASCRIPT's leniency

## Good

```js
// GOOD
var a = 10;
a = 5;
```

## Bad

`var` can be obmitted
```js
a = 10;
```

`var` can be repeated
```js
var a = 10;
var a = 12;
```

=> Avoid.

---

# Scope

`GLOBAL` vs `LOCAL`

```js
var a = 10;     // global

function fn() {
    var a = 10; // local
}
```

When a variable is referenced without the `var` keyword in a function, javascript does the following : 

1. Looks for a **local** variable
2. Looks for a **global** variable
3. Create a **global** variable

---

## Example 1

```js
function fn() {
    a = 20;
}

var a = 10;
fn();
console.log(a);
```

---

## Example 2

```js
function fn() {
    var a = 20;
}

var a = 10;
fn();
console.log(a);
```

---

## Example 3

```js
function fn(a) {
    a = 20;
}

var a = 10;
fn(a);
console.log(a);
```

---

## Example 4

```js
function fn(a) {
    a = 20;
    var b = 30;
    return b;
}
var a = 10;
a = fn(a);
console.log(a);
```

---

# Defining a function

```js
function add(a, b) {
    return a + b;
}
```

```js
var add = function(a, b) {
    return a + b;
}
```

---

## Difference

```js
fn();

function fn() {
    console.log(1);
}

fn();
```

Both works

---

## Difference

```js
fn();

var fn = function() {
    console.log(1);
}

fn();
```

`fn()` cannot be called before the variable is declared.

---

# Function as a variable

```js
var fn = function() {  
    console.log('hello') 
}

var fn2 = function(f) {
    f();
    f();
}

fn2(fn);
```

---

# Array of functions

```js
var fn1 = function () { console.log(1); }
var fn2 = function () { console.log(2); }
var fn3 = function () { console.log(3); }

var ar = [fn1, fn2, fn3];

for (var i = 0; i < ar.length; i++) {
    ar[i]();
}
```
