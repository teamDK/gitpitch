# Loop Homework Quizzes

### Q1

Print the following

```language-none
score : 10
score : 20
score : 30
score : 40
score : 50
score : 60
score : 70
score : 80
score : 90
```

### Q2

Print the following (use the answer from Q1)

```language-none
score : 10
score : 20
score : 30
score : 40
score : 50
--------------------
score : 60
score : 70
score : 80
score : 90
```

### Q3

Calculate the sum of all numbers in the following series

```language-none
2, 4, 6, 8, 10, 12, 14, 16, 18, 20
```

### Q4

Calculate the average of all numbers 1 through 10

### Q5 (hard)

Find the biggest number that can divide 56