# Intermediate

Use the following array for all questions.

```js
var ar = [1, 5, 2, 4, 3, 10, 8, 3]
```

### Q1.

Calculate  the sum of all numbers *excluding* the min and the max.

### Q2.

Find the median

### Q3.

Find the second largest number

### Q4.

Find the average of even numbers

### Q5. 

Find a number that occurs more than once