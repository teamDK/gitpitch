Q1. Review slides

Q2. Given a number variable `a`, print `even` or `odd` based on its oddity.

Q3. Given a string variable `color`, print the colors elemental power based on the table below. Use a `switch` statement.

| color | elemental power |
|---|---|
| red | fire |
| blue | ice |
| black | earth |
| green | nature |
| white | thunder |

Q4. Given a number variable `num`, print one of the following 4 outcomes based on the number.

1. Less than 3
2. Between 3 and 10
3. Greater than 10