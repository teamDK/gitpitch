Given the following schema `quiz`

<img src="https://firebasestorage.googleapis.com/v0/b/project-3068490203370471426.appspot.com/o/pt%2Fimg%2Fnba-schema.png?alt=media&token=ddf74fdd-148b-4ac1-add8-281d87c6b3f1" style="width: 100%">

## Top 5 winningest teams of all time (Season)

```language-none
Bulls	2001	69	11
Bulls	2002	67	13
Thunder	2001	67	13
Suns	2002	58	22
Suns	2000	53	27
Thunder	2002	49	31
```

## Top 5 Winningest teams of all time (Accumulated)

```language-none
Bulls		149	91
Thunder		132	108
Suns		122	118
Lakers		112	128
```

## Highest scoring players of all time (season avg)

```language-none
2001	dennis rodman	34.7143
2000	dennis rodman	34.1791
2000	michael jordan	31.8667
2001	lebron james	31.5286
2000	albert pujols	29.8209
2001	kobe bryant	26.6269
2002	albert pujols	26.4407
```

## Highest scoring players of all time (career avg)

```language-none
dennis rodman	26.6381
kobe bryant	22.6927
albert pujols	22.3600
lebron james	22.2821
scottie pippen	21.5438
```


## Season Standings of 2001 season

## Assist leader for 2002 season (with team names)

```language-none
mark mcgwuire	Thunder	2002	12.0000
lebron james	Suns	2002	11.9254
dennis rodman	Bulls	2002	11.6712
shaquille oneal	Lakers	2002	9.7321
albert pujols	Thunder	2002	9.2203
...
```

## List of all teams who won 50% or more

## Each players career high seasons (in points / game)

```language-none
michael jordan	31.8667
scottie pippen	25.0429
dennis rodman	34.7143
kobe bryant	26.6269
shaquille oneal	26.1081
...
```

## Highest scoring players for each team in 2002

```language-none
Bulls	2002	scottie pippen
Lakers	2002	kobe bryant
Suns	2002	kevin love
Thunder	2002	albert pujols
```