# Node.js

---

## Features

- Asynchronous and Event Drive
- Very Fast
- Single Threaded
- Highly Scalable
- No Buffering
- License

---

## Where to use?

- I/O bound Applications
- Data Streaming Applications
- Data Intensive Real-time Applications (DIRT)
- JSON APIs based Applications
- Single Page Applications

## Where not to use?

- CPU intensive applications

---

# Installation

## Mac

```bash
% brew install node
% node -v
% npm -v
```

## PC

```bash
c:\> choco install nodejs.install
c:\> choco upgrade nodejs.install
```

--- 

# Command-Line 

**R**ead-**E**val-**P**rint-**L**oop

```bash
% node
> console.log("hello world!");
```

```bash
> 1+1
2
> 3+3
6
> function a() {
... console.log(123);
... }
> a()
123
```

---

# File

```js
// hello.js
console.log("hello world!");
```

```bash
% node hello.js
```

---

# Async & Callbacks

---

## Synchronous

Wait for 1 second than print "done waiting"

```js
function setTimeoutSync() {
    var dt = new Date();
    while ((new Date()) - dt <= 1000);
    console.log("done waiting!");
}

setTimeoutSync();
console.log("do something else");
```

---

## Asynchronous 

```js
setTimeout(function() {
  console.log("done waiting")
}, 1000);
console.log("do something else");
```

The anonymous function provided as the parameter is called a `callback` function.

---

# Callback - Node.js Convention

## Declaration

```js
function doSomething(callback) {
    if (/*there is an error*/) {
        var err = new Error("something went wrong");
        callback(err);
    } else {
        callback(null, result);
    }
}
```

## Usage 

```js
doSomething(function(err, data) {
    if (err) {
        console.error(err.message);
    } else {
        console.log(data);
    }
});
```

---

# Writing a server application

---

## First Server

```js
// main.js
var http = require("http");

var server = http.createServer(reqHandler);

server.listen(3000);

function reqHandler(req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end("Hello World");
}

console.log("Server listening on port 3000");
```

```bash
$ node main.js
```

---

## Anonymous function

```js
// main.js
var http = require("http");

var server = http.createServer(function(req, res) {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end("Hello World");
}).listen(3000);

console.log("Server listening on port 3000");
```

---

# Using a module

---

## File System Module 'fs'

```js
var http = require("http");
var fs = require("fs");

http.createServer(function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    var data = fs.readFileSync("index.html");
    response.end(data);
}).listen(8081);
```

Problem?

---

## Non-Blocking way

Use a callback

```js
var http = require("http");
var fs = require("fs");

http.createServer(function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/html' });
    fs.readFile("index.html", function (err, data) {
        response.end(data.toString());
    });
}).listen(8081);
```

--- 

# Writing a Custom Module

---

## Importing a module

```js
var myModule = require('path');
```

---

## Defining a module

A module exports an object namely the `module.exports`

```js
var module = { exports: {} };
var exports = module.exports;

// file content

return module.exports;
```

---

## Example - Random Int Generator Module

```js
var rand = require('./randomInt');

rand.getRandInt(10);
rand.getRandRange(10, 20);
```

```js
// var module = { exports: {} }'
// var exports = module.exports;

module.exports = {
    getRandInt: function(max) {
        return Math.floor(Math.random() * max)
    }

    getRandRange: function(min, max) {
        Math.floor(Math.random() * (max - min)) + min;
    }
}

// return module.exports
```

---

## Exercise

Write a module which will create a random array and return it.

```js
var randArray = require('array-module');

console.log(randArray.gen(10));     // should print something like [1, 13, 15, 2, 19]
```

---

## Quiz

Each block of code represents a separate module to be imported.  

1. Discuss its validity.
2. If invalid, devise a fix.
3. Discuss the usage.

```js
module = 10
```

```js
module.exports = 10;
```

```js
exports = 10;
```

```js
module.exports = { num: 10 }
```

```js
exports = { num: 10 }
```

```js
exports = function() {
    console.log("hi");
}
```

```js
module.exports = function(n) {
    console.log(n);
}
```

```js
exports.add(n) {
    console.log(n);
}
```

```js
exports.sum = function (a, b) {
    return a + b;
}
```

```js
exports.fn1 = function() { console.log(1) }
module.exports.fn2 = function() { console.log(2) }
```

```js
function print() {
    console.log(10);
}

module.exports = print;
```

```js
exports.seed = 1;

function printSeed() {
    console.log(this.seed);
}

module.exports = printSeed;
```

```js
var name = 'dan'

module.exports = function() { console.log(name) }
```

```js
var var1 = 10;
var fn1 = function() { console.log(this.var1) }
exports.fn1 = fn1;
```

---

## Best Practice

```js
var var1 = 10;      // public
var var2 = 20;      // private
var fn1 = function() { console.log(var1) }
var fn2 = function() { console.log(var2) }

exports.var1 = var1;
exports.fn1 = fn1;
exports.fn2 = fn2;
```

```js
module.fn1();
console.log(module.var1);

module.fn2();
console.log(module.var2);   // undefined
```

---

## Best Practice 2

```js
var var2 = 20;      // private

exports.var1 = 10;      // public

exports.fn1 = function() { console.log(this.var1) }
exports.fn2 = function() { console.log(var2) }
```

---

## Best Practice 3

```js
module.exports = {
    name: 'dan',
    print: function() {
        //...
    }
}
```

---

# Sample Server

```js
var http = require("http");
var fs = require("fs");

var server = http.createServer((req, res) => {
    if (req.url == "/") {
        serveStatic('/index.html', res);
    }
    else if (/\.(html|css|js)$/i.test(req.url)) {
        serveStatic(req.url, res);
    }
    else {
        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end('Requested resource does not exist');
    }
}).listen(3000);

console.log("Server listening on port 3000");

function serveStatic(url, res) {
    var ext = url.split('.').pop()
    var filename = url.split('/').pop();

    var contentType = 'text/plain';
    switch(ext) {
        case 'html': contentType = 'text/html'; break;
        case 'css': contentType = 'text/css'; break;
        case 'js' : contentType = 'application/javascript'; break;
    }

    res.writeHead(200, { 'Content-Type': contentType });
    fs.readFile(filename, function(err, data) {
        res.end(data);
    })
}
```