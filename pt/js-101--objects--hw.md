# JS 101 - Objects Homework

Complete the blank object `person` so that the code works as desired.

Submit the entire code in one file.

```js
var person = {
    // fill in here
}

person.printFavoriteColors();

console.log("-----------------");

person.favoriteColors[2] = 'Orange'
person.printFavoriteColors();
```

**expected output**
```language-none
Red
Yellow
Blue
-----------------
Red
Yellow
Orange
```
